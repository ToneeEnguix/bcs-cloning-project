// DESCRIPTION //

This repo was made as part of the Barcelona Code School Full-Stack bootcamp.

It is a clone of their website: barcelonacodeschool.com using only HTML and CSS.

Most of the links are working and they will redirect you to their actual site.

Their font-family is pay-to-use so I had to use another one, which makes the resembling less accurate. 

In conclusion, I found this cloning to be very useful for the basic understanding of HTML + CSS.


// LIVE DEMO //

www.barcelonacodeschool.surge.sh